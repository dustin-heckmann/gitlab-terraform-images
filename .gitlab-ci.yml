---
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_REF_PROTECTED == 'true'

variables:
  BASE_IMAGE: "alpine:3.16.2"
  BUILD_IMAGE_NAME: "$CI_REGISTRY_IMAGE/branches/$CI_COMMIT_REF_SLUG-$TERRAFORM_VERSION:$CI_COMMIT_SHA"
  DOCKER_BUILDKIT: "1"
  PLATFORMS: linux/amd64,linux/arm64
  RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/releases/$TERRAFORM_VERSION"
  STABLE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/stable:latest"
  STABLE_VERSION: "1.1"
  TF_ADDRESS: "$CI_API_V4_URL/projects/$CI_PROJECT_ID/terraform/state/$CI_PIPELINE_IID-$STATE_NAME"

.versions:
  parallel:
    matrix:
      - TERRAFORM_BINARY_VERSION: "1.2.6"
        TERRAFORM_VERSION: "1.2"
        STATE_NAME: terraform12
      - TERRAFORM_BINARY_VERSION: "1.1.9"
        TERRAFORM_VERSION: "1.1"
        STATE_NAME: terraform11
      - TERRAFORM_BINARY_VERSION: "1.0.11"
        TERRAFORM_VERSION: "1.0"
        STATE_NAME: terraform10

stages:
  - lint
  - build
  - test-integration
  - test-init
  - test-fmt
  - test-validate
  - test-plan
  - test-apply
  - test-destroy
  - prepare-release
  - release
  - upload-test-module

shell check:
  stage: lint
  image: koalaman/shellcheck-alpine:stable
  before_script:
    - shellcheck --version
  script:
    - shellcheck src/**/*.sh

commit lint:
  stage: lint
  variables:
    GIT_DEPTH: "0"
  needs: []
  image: node:16
  cache:
    key:
      files:
        - package.json
    paths:
      - node_modules
  before_script:
    - npm install
  script:
    - npx --quiet commitlint --from="$CI_MERGE_REQUEST_DIFF_BASE_SHA" --help-url 'https://gitlab.com/gitlab-org/terraform-images/-/blob/master/CONTRIBUTING.md'
  rules:
    - if: "$CI_MERGE_REQUEST_EVENT_TYPE != 'merge_train' && $CI_MERGE_REQUEST_DIFF_BASE_SHA"

dockerfile check:
  stage: lint
  image: hadolint/hadolint:latest-alpine
  before_script:
    - hadolint --version
  script:
    - hadolint Dockerfile

build:
  extends: .versions
  stage: build
  services:
    - docker:20.10.12-dind
  image: docker:20.10.12-dind
  before_script:
    # Install buildx
    - mkdir -p ~/.docker/cli-plugins
    - wget https://github.com/docker/buildx/releases/download/v0.7.1/buildx-v0.7.1.linux-amd64 -O ~/.docker/cli-plugins/docker-buildx
    - chmod a+x ~/.docker/cli-plugins/docker-buildx
    # See https://www.docker.com/blog/multi-platform-docker-builds/
    - docker run --rm --privileged docker/binfmt:a7996909642ee92942dcd6cff44b9b95f08dad64
    # Registry auth
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker buildx create --use
    - docker buildx build
      --platform "$PLATFORMS"
      --build-arg BASE_IMAGE=$BASE_IMAGE
      --build-arg TERRAFORM_BINARY_VERSION=$TERRAFORM_BINARY_VERSION
      --file Dockerfile
      --tag "$BUILD_IMAGE_NAME"
      --push
      .

upload:
  stage: upload-test-module
  script:
    - cd tests/modules/random-pet
    - tar -cvzf ${CI_PROJECT_NAME}-local-${TERRAFORM_MODULE_VERSION}.tgz -C ${CI_PROJECT_DIR} .
    - 'curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ${CI_PROJECT_NAME}-local-${TERRAFORM_MODULE_VERSION}.tgz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/terraform/modules/random-pet/local/${TERRAFORM_MODULE_VERSION}/file'
  variables:
    TERRAFORM_MODULE_VERSION: 0.0.1
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab-org/terraform-images" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: manual

.test-base:
  image: "$BUILD_IMAGE_NAME"
  before_script:
    - gitlab-terraform version
    - jq --version
  cache:
    key: "$TERRAFORM_VERSION-$CI_COMMIT_REF_SLUG"
    paths:
      - tests/.terraform/

.test:
  extends:
    - .test-base
  before_script:
    - !reference [.test-base, before_script]
    - cd tests

.test-tf-root:
  extends:
    - .test-base
  variables:
    TF_ROOT: tests

test-init:
  extends:
    - .test
    - .versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init

test-init-with-args:
  extends:
    - .test
    - .versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init -get=true -no-color

test-init-with-flags:
  extends:
    - .test
    - .versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - export TF_INIT_FLAGS="-get=true -no-color"
    - gitlab-terraform init

test-init-with-flags-and-args:
  extends:
    - .test
    - .versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - export TF_INIT_FLAGS="-get=true"
    - gitlab-terraform init -no-color

test-init-tf-root:
  extends:
    - .test-tf-root
    - .versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init

test-init-tf-root-with-cd:
  extends:
    - .test-tf-root
    - .versions
  stage: test-init
  script:
    - cd tests
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init

test-init-tf-root-with-args:
  extends:
    - .test-tf-root
    - .versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init -get=true -no-color

test-init-tf-root-with-flags:
  extends:
    - .test-tf-root
    - .versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - export TF_INIT_FLAGS="-get=true -no-color"
    - gitlab-terraform init

test-init-tf-root-with-flags-and-args:
  extends:
    - .test-tf-root
    - .versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - export TF_INIT_FLAGS="-get=true"
    - gitlab-terraform init -no-color

test-fmt:
  extends:
    - .test
    - .versions
  stage: test-fmt
  script:
    - gitlab-terraform fmt

test-validate:
  extends:
    - .test
    - .versions
  stage: test-validate
  script:
    - gitlab-terraform validate

test-plan:
  extends:
    - .test
    - .versions
  stage: test-plan
  variables:
    TF_PLAN_CACHE: $TERRAFORM_VERSION-plan.cache
  script:
    - gitlab-terraform plan
    - if [[ ! -f "$TERRAFORM_VERSION-plan.cache" ]]; then echo "expected to find a plan.cache file"; exit 1; fi
    - gitlab-terraform plan-json
    - if [[ ! -f "plan.json" ]]; then echo "expected to find a plan.json file"; exit 1; fi
  artifacts:
    paths:
      - "tests/*-plan.cache"

test-apply:
  extends:
    - .test
    - .versions
  stage: test-apply
  variables:
    TF_PLAN_CACHE: $TERRAFORM_VERSION-plan.cache
  script:
    - gitlab-terraform apply

test-destroy:
  extends:
    - .test
    - .versions
  stage: test-destroy
  script:
    - gitlab-terraform destroy

integration-test:
  stage: test-integration
  variables:
    TERRAFORM_VERSION: $STABLE_VERSION
    TF_STATE_NAME: default
    TF_CACHE_KEY: default
    TF_ROOT: tests
  trigger:
    include: .gitlab/integration-test/Test-Terraform.gitlab-ci.yml
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab-org/terraform-images"'
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"'

release:
  extends: .versions
  stage: release
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    # https://github.com/google/go-containerregistry/blob/main/cmd/crane/doc/crane_copy.md
    - crane auth login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - crane copy "$BUILD_IMAGE_NAME" "$RELEASE_IMAGE_NAME:latest"
    - crane copy "$BUILD_IMAGE_NAME" "$CI_REGISTRY_IMAGE/releases/terraform:$TERRAFORM_BINARY_VERSION"
    - crane copy "$BUILD_IMAGE_NAME" "$RELEASE_IMAGE_NAME:$CI_COMMIT_TAG"
    - if [ "$TERRAFORM_VERSION" = "$STABLE_VERSION" ]; then crane copy "$BUILD_IMAGE_NAME" "$STABLE_IMAGE_NAME"; fi
  rules:
    - if: $CI_COMMIT_TAG

.semantic-release:
  image: node:16-buster-slim
  stage: prepare-release
  before_script:
    - apt-get update && apt-get install -y --no-install-recommends git-core ca-certificates
    - npm install -g semantic-release @semantic-release/gitlab
  script:
    - semantic-release $DRY_RUN_OPT -b $CI_COMMIT_REF_NAME

tag_release-dryrun:
  extends: .semantic-release
  variables:
    DRY_RUN_OPT: "-d"
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab-org/terraform-images" && $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'

tag_release:
  extends: .semantic-release
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab-org/terraform-images" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
